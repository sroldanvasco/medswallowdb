# MedSwallowDB



Esta es la versión en español de las variables clínicas recolectadas en el proyecto titulado "Diagnóstico y seguimiento de pacientes con disfagia neuromuscular y neurogénica mediante la integración de señales no invasivas y variables clínicas". Proyecto financiado por MinCiencias (Colombia) convocatoria 777 de 2017. Número contrato Colciencias 825-2017. Institución ejecutora: Universidad Pontificia Bolivariana. Instituciones co ejecutoras: Instituto Tecnológico Metropolitano y Organización Fonoaudiológica OFA IPS.

Esta base de datos que posee 158 variables de tipo clínico agrupadas en características sociodemográficas y antecedentes, características de le deglución y síntomas de disfagia orofaríngea neurogénica, signos al examen clínico de la deglución y hallazgos al testo oral con consistencias/volumen. Son 166 personas, distribuidas en 86 pacientes (casos o pacientes con disfagia orofaríngea neurogénica de causas neurológicas y neuromusculares) y 80 controles (personas sanas sin disfagia). 


## Hoja de ruta
Está pendiente la publicación de la base de datos de señales y características de electromiografía de superficie, auscultación cervical mediante acelerometría, y voz/habla

## Contribuciones
Los datos fueron recolectados en la Organización Fonoaudiológica E.U. (OFA IPS), Medellín, Colombia. Además, las siguientes personas contribuyeron a la recolección de datos:

- Lillyana Martínez Moreno, OFA IPS (Colombia)

- Estefanía Pérez-Giraldo, MSc., Instituto Tecnológico Metropolitano de Medellín (Colombia)

- Juan Pablo Restrepo-Uribe, MSc., Instituto Tecnológico Metropolitano de Medellín (Colombia), Universidad de Envigado (Colombia)

- Rafael Orozco-Arroyave, MSc., PhD., Universidad de Antioquia (Colombia), Friedrich-Alexander Universität Erlangen-Nürnberg (Alemania)

- Zulma Vanessa Rueda-Vallejo, MD., PhD., University of Manitoba (Canadá)


## Autores

- Juan Camilo Suárez-Escudero, MD., PhD., Universidad Pontificia Bolivariana (Colombia), Grupo de Investigación en Salud Pública, juanca.suarez@upb.edu.co

- Andrés Felipe Orozco-Duque, PhD.,Universidad de Medellín (Colombia), Grupo de Investigación ARKADIUS, aforozco@udem.edu.co

- Sebastián Roldán-Vasco, MSc., PhD., Instituto Tecnológico Metropolitano de Medellín (Colombia), Grupo de Materiales Avanzados y Energía, sebastianroldan@itm.edu.co


## Estado del proyecto
Actualmente se encuentran consignadas solo las variables clínicas recolectadas. Para acceso y uso de señales de electromiografía, acelerometría y voz/habla, debe contactar a:

- Sebastián Roldán-Vasco, MSc, PhD. Docente investigador del Instituto Tecnológico Metropolitano de Medellín (Colombia), Grupo de Materiales Avanzados y Energía, sebastianroldan@itm.edu.co